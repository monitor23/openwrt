*** Settings ***
Documentation      Robot Framework test script
Library            SSHLibrary

*** Variables ***
${host}            192.168.1.1
${username}        root
${password}        adsl55
${alias}           remote_host_1

*** Test Cases ***
Test SSH Connection
    Open Connection     ${host}        alias=${alias}
    Login               ${username}    ${password}    delay=1
    Execute Command     hostname
    Close All Connections